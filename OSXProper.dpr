//Only compile for the OS X platform.
{$IF ( (NOT Defined(MACOS)) OR (Defined(IOS)) )}
  {$MESSAGE Fatal 'OSXProper.dpr only compiles for the OS X platform.'}
{$ENDIF}

// ******************************************************************
//
// Program Name   : OSXProper
// Platform(s)    : OSX
// Framework      : FMX
//
// Filename       : OSXProper.dpr
// File Version   : 1.00
// Date Created   : 16-Sep-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Demo project showing the proper way to create a Mac OS X
// application.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   This is a demo project showing the proper way to create a Mac
///   OS X application using Delphi and the Firemonkey <br />
///   framework.
/// </summary>
/// <remarks>
///   <para>
///     Please feel free to use this project as a template to
///     create your own Mac OS X projects using Delphi.
///   </para>
///   <para>
///     If you use this project credit to Angelic Technology would
///     be greatly appreciated (although it is not required.)
///   </para>
///   <para>
///     Project documentation was created using Documentation
///     Insight from <see href="http://www.devjetsoftware.com/?utm_source=trial&amp;utm_medium=app">
///     DevJet Software</see>
///   </para>
/// </remarks>
program OSXProper;

uses
  System.StartUpCopy,
  FMX.Forms,
  AT.OSXProper.FMX.Forms.Controller in 'Forms\AT.OSXProper.FMX.Forms.Controller.pas' {frmController},
  AT.OSXProper.DM.Actions in 'Data Modules\AT.OSXProper.DM.Actions.pas' {dmActions: TDataModule},
  AT.OSXProper.FMX.Forms.Main in 'Forms\AT.OSXProper.FMX.Forms.Main.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TdmActions, dmActions);
  Application.CreateForm(TfrmController, frmController);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
