# OSXProper README #

This repository contains a demo project showing the proper way to
create a Mac OS X application using Delphi and the Firemonkey
framework.

Please feel free to use this project as a template to create your own
Mac OS X projects using Delphi.

If you use this project credit to Angelic Technology would be greatly
appreciated (although it is not required.)

Project documentation was created using Documentation Insight from
DevJet Software http://www.devjetsoftware.com/?utm_source=trial&amp;utm_medium=app
