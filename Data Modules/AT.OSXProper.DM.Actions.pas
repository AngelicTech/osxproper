//Only compile for the OS X platform.
{$IF ( (NOT Defined(MACOS)) OR (Defined(IOS)) )}
  {$MESSAGE Fatal 'AT.OSXProper.DM.Actions.pas only compiles for the OS X platform.'}
{$ENDIF}

// ******************************************************************
//
// Program Name   : OSXProper
// Platform(s)    : OSX
// Framework      : FMX
//
// Filename       : AT.OSXProper.DM.Actions.pas/.dfm
// File Version   : 1.00
// Date Created   : 16-Sep-2017
// Author         : Matthew Vesperman
//
// Description:
//
// This data module defines the application's actions and their
// associated logic.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Defines the application's actions data module.
/// </summary>
unit AT.OSXProper.DM.Actions;

interface

uses
  System.SysUtils, System.Classes, FMX.StdActns, System.Actions,
  FMX.ActnList;

type
  /// <summary>
  ///   This data module defines the application's actions and their
  ///   associated logic..
  /// </summary>
  TdmActions = class(TDataModule)
    actAppAbout: TAction;
    actAppExit: TFileExit;
    actAppHide: TFileHideApp;
    actAppHideOthers: TFileHideAppOthers;
    actAppPreferences: TAction;
    alstMain: TActionList;
  strict protected
    /// <summary>
    ///   Initializes the action objects.
    /// </summary>
    /// <remarks>
    ///   This is typically used to assign shortcuts that cannot be
    ///   added in the visual designer.
    /// </remarks>
    procedure InitializeActions;
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
  dmActions: TdmActions;

implementation

{%CLASSGROUP 'FMX.Controls.TControl'}

uses
  FMX.Consts;

{$R *.dfm}

constructor TdmActions.Create(AOwner: TComponent);
begin
  inherited;

  InitializeActions;
end;

procedure TdmActions.InitializeActions;
begin
  //Create Cmd+, shortcut and assign it to the Preferences action.
  actAppPreferences.ShortCut := TextToShortcut(SmkcCmd + ',');

  {*
    NOTE: TextToShortcut is broken in FMX (at the time of this
          writing) and you cannot assign shortcuts using a string
          such as "Cmd+A", Instead you must use the constants in
          FMX.Consts to specify the modifier key (ie: Cmd, Alt,
          Shift).
  *}
  { TODO : Initialize additional actions here. }
end;

end.
