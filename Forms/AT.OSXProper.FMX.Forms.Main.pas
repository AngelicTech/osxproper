//Only compile for the OS X platform.
{$IF ( (NOT Defined(MACOS)) OR (Defined(IOS)) )}
  {$MESSAGE Fatal 'AT.OSXProper.FMX.Forms.Main.pas only compiles for the OS X platform.'}
{$ENDIF}

// ******************************************************************
//
// Program Name   : OSXProper
// Platform(s)    : OSX
// Framework      : FMX
//
// Filename       : AT.OSXProper.FMX.Forms.Main.pas/.fmx
// File Version   : 1.00
// Date Created   : 16-Sep-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Defines the mai UI window class.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Defines the mai UI window class.
/// </summary>
unit AT.OSXProper.FMX.Forms.Main;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics,
  FMX.Dialogs;

type
  /// <summary>
  ///   This class defines the main UI window for the application.
  /// </summary>
  TfrmMain = class(TForm)
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.fmx}

end.
