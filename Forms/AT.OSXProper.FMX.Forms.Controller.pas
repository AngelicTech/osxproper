//Only compile for the OS X platform.
{$IF ( (NOT Defined(MACOS)) OR (Defined(IOS)) )}
  {$MESSAGE Fatal 'AT.OSXProper.FMX.Forms.Controller.pas only compiles for the OS X platform.'}
{$ENDIF}

// ******************************************************************
//
// Program Name   : OSXProper
// Platform(s)    : OSX
// Framework      : FMX
//
// Filename       : AT.OSXProper.FMX.Forms.Controller.pas/.fmx
// File Version   : 1.00
// Date Created   : 16-Sep-2017
// Author         : Matthew Vesperman
//
// Description:
//
// Defines the main controller window for a Mac OS X application.
//
// Revision History:
//
// v1.00   :   Initial version
//
// ******************************************************************
//
// COPYRIGHT � 2017 - PRESENT Angelic Technology
// ALL RIGHTS RESERVED WORLDWIDE
//
// ******************************************************************

/// <summary>
///   Defines the main controller window for a Mac OS X application.
/// </summary>
unit AT.OSXProper.FMX.Forms.Controller;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants, FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics,
  FMX.Dialogs, FMX.Menus, Macapi.AppKit;

type
  /// <summary>
  ///   This form class defines the main controller window for a Mac
  ///   OS X application.
  /// </summary>
  /// <remarks>
  ///   <para>
  ///     The controller form holds the application's main menu and
  ///     allows the application to behave in a manner that Mac
  ///     users expect.
  ///   </para>
  ///   <para>
  ///     The Application object will see this as the main form,
  ///     however the user will see the TfrmMain window as the main
  ///     form.
  ///   </para>
  ///   <para>
  ///     Proper techniques should be employed to reference the
  ///     correct window in your code.
  ///   </para>
  /// </remarks>
  TfrmController = class(TForm)
    mniApp: TMenuItem;
    mniAppAbout: TMenuItem;
    mniAppExit: TMenuItem;
    mniAppHide: TMenuItem;
    mniAppHideOthers: TMenuItem;
    mniAppPreferences: TMenuItem;
    mniAppSep1: TMenuItem;
    mniAppSep2: TMenuItem;
    mniAppSep3: TMenuItem;
    mniHelp: TMenuItem;
    mnuMain: TMainMenu;
    /// <summary>
    ///   The form's OnShow event handler.
    /// </summary>
    /// <remarks>
    ///   The form must hide itself from the Mac OS X Show All
    ///   Windows command from the OnShow event handler or it doesn't
    ///   work.
    /// </remarks>
    procedure FormShow(Sender: TObject);
  strict protected
    /// <summary>
    ///   Returns a NSWindow reference for this window.
    /// </summary>
    /// <returns>
    ///   A reference to a NSWindow interface.
    /// </returns>
    function FormNSWindow: NSWindow;
    /// <summary>
    ///   Hides this window from Mac OS X's Show All Windows command.
    /// </summary>
    procedure HideFromShowAll;
  public
    constructor Create(AOwner: TComponent); override;
  end;

var
  frmController: TfrmController;

implementation

{$R *.fmx}

uses
  FMX.Platform.Mac, AT.OSXProper.DM.Actions;

constructor TfrmController.Create(AOwner: TComponent);
begin
  inherited;

  //Turn off all border icons.
  BorderIcons := [];

  //Turn off the border.
  BorderStyle := TFmxFormBorderStyle.None;

  //Make window transparent.
  Transparency := True;

  //Make window 0x0 pixels in size and move it to the top left corner
  //of the screen.
  SetBounds(0, 0, 0, 0);
end;

function TfrmController.FormNSWindow: NSWindow;
var
  AHandle: TMacWindowHandle;
begin
  //Get the Mac OS X handle for this window.
  AHandle := WindowHandleToPlatform(Self.Handle);

  //Return its NSWindow interface.
  Result := AHandle.Wnd;
end;

procedure TfrmController.FormShow(Sender: TObject);
begin
  HideFromShowAll;
end;

procedure TfrmController.HideFromShowAll;
var
  AWindow: NSWindow;
begin
  //Get the NSWindow interface reference.
  AWindow := FormNSWindow;

  //Set the window to transient mode.
  //In transient mode the window floats in Spaces and is hidden by
  //Expos�.
  AWindow.setCollectionBehavior(AWindow.collectionBehavior OR
      NSWindowCollectionBehaviorTransient);
end;

end.
